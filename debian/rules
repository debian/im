#!/usr/bin/make -f

PACKAGE=im

IM_RELEASE_VERSION=`head -1 00changes | cut -d ' ' -f 1`
IM_RELEASE_DATE=`head -1 00changes | cut -d ' ' -f 2 | sed 's/[()]//g; s:/:-:g'`

build: build-arch build-indep

build-arch:

build-indep: build-indep-stamp
build-indep-stamp:
	dh_testdir
	dh_update_autotools_config
	./configure --prefix=/usr --sysconfdir=/etc \
	  --with-perldir=/usr/share/perl5 --with-ssh=/usr/bin/ssh \
	  --with-hostname=invalid.example.jp --with-fsyncno=0 \
	  --disable-rpop --disable-usecl
	touch $@

clean:
	dh_testdir
	dh_testroot
	rm -f *-stamp pod2htm*
	if [ -f Makefile ]; then make distclean; fi
	debconf-updatepo
	dh_clean

install: install-arch install-indep

install-arch:

install-indep: install-indep-stamp
install-indep-stamp:
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs

	make DESTDIR=$(CURDIR)/debian/$(PACKAGE) install

	cp man/00readme debian/$(PACKAGE)/usr/share/doc/im/manual/
	cp man/imput debian/$(PACKAGE)/usr/share/doc/im/manual/en/
	cp man/*.jis debian/$(PACKAGE)/usr/share/doc/im/manual/ja/

	for i in Address Alias Config EncDec File \
	  Folder GetPass Grep History Http \
	  Imap Iso2022jp Japanese LocalMbox Log \
	  MD5 Message MsgStore Nntp Pop \
	  Recipient Scan Smtp Ssh Stdio \
	  TcpTransaction Util; do \
	 pod2html IM/$$i.pm \
	   > debian/$(PACKAGE)/usr/share/doc/im/html/IM::$$i.html; \
	 pod2man --section=3pm --release=IM$(IM_RELEASE_VERSION) \
	   --date=$(IM_RELEASE_DATE) \
	   --center="Internet Message Perl modules" \
	   IM/$$i.pm > debian/$(PACKAGE)/usr/share/man/man3/IM::$$i.3pm; \
	done

	for i in imali imcat imcd imclean imget \
	  imgrep imhist imhsync imjoin imls \
	  immknmz immv impack impath imput \
	  impwagent imrm imsetup imsort imstore \
	  imtar; do \
	 pod2man --section=1p --release=IM$(IM_RELEASE_VERSION) \
	   --date=$(IM_RELEASE_DATE) \
	   --center="Internet Message commands" \
	   $$i > debian/$(PACKAGE)/usr/share/man/man1/$$i.1p; \
	done

	rm -f debian/$(PACKAGE)/etc/im/SiteConfig
	touch $@

binary-indep: build-indep install-indep
	dh_testdir
	dh_testroot
	dh_installdocs
	dh_installexamples
#	dh_installmanpages
	dh_installchangelogs 00changes
	dh_strip
	dh_compress
#	dh_suidregister
	dh_fixperms
	dh_installdebconf
	dh_installdeb
	dh_perl
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary-arch: build-arch install-arch

binary: binary-arch binary-indep
.PHONY: build-indep build-arch build clean binary-indep binary-arch binary install-indep install-arch install
